## Instalação com docker 

- Clone o projeto
```
git clone git@gitlab.com:wllcee/full-stack-test.git
```
- Entre na raiz do prejeto e rode:
```
docker-compose up -d --build
```

## BACKEND
- Entre no terminal do container ```backend```, com comando:
 ```
 docker exec -it backend bash
 ```
- no terminal do container rode:

```
composer install
```
- logo após, rode esses comandos na pasta backend ```(fora do terminal do container)```
```
cp .env.example .env
```
```
php artisan key:generate
```

- Não esqueça de configurar o banco de dados na ``` .env ```
  
![image](https://github.com/Wallacewss2033/fullstack-challenge-20231205/assets/39920409/ec726dce-7762-4c68-b66c-668698afad41)

OBS: user e senha do mysql ambos são ```root```, o banco é ```watchbrasil``` e o host é ```mysql-watch```

- Volte para o terminal do container. Para criar o banco de dados rode:
```
php artisan migrate
```

- Para popular a tabela de accounts
```
php artisan db:seed
```

OBS: CASO HAJA ALGUM PROBLEMA DE PERMISSÃO NO PROJETO RODE:

```
chown -R root:www-data storage
```
```
chmod -R 777 ./storage
```
```
chmod -R 775 ./storage
```

## FRONTEND
- O frontend já estará em execução, quando jogar o docker buildar.

- Apenas configure a ```.env```

- Para a variável de ambiente ```REACT_APP_BASE_IMAGE_URL``` use o valor ```"https://br.web.img3.acsta.net/c_310_420/pictures/"```
    
