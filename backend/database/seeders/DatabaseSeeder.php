<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Movie;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {

        User::factory()->create([
            'name'     => 'Wallace Souza',
            'email'    => 'wallace@hotmail.com',
        ]);
        
        User::factory(29)->create();

        Movie::factory(30)->create();
    }
}
