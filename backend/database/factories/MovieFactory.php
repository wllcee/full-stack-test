<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Movie>
 */
class MovieFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title'             => 'The ' . fake()->colorName() . ' ' . fake()->name(),
            'description'       => fake()->text(),
            'image'             => fake()->randomElement(['21/10/28/21/10/2697497.jpg', '18/06/14/20/53/4734164.jpg', '16/01/21/18/13/267664.jpg', '14/07/09/16/27/398352.jpg', '/23/08/22/16/44/1620470.jpg']),
            'classification'    => fake()->randomElement(['L', '10', '12', '18']),
            'release_year'      => fake()->dateTimeBetween()->format('Y'),
        ];
    }
}
