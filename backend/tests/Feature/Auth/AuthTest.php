<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    protected string $name          = 'Wallace Souza';
    protected string $email         = 'wallace@teste.com';
    protected string $password      = 'password';

    public function setUp(): void
    {
        parent::setUp();
        User::factory()->create([
            'name'      => $this->name,
            'email'     => $this->email,
        ]);
    }

    public function test_endpoint_login(): void
    {
        $data = [
            'email'    => $this->email,
            'password' => $this->password,
        ];

        $response = $this->post('/api/login', $data);
        $this->assertTrue($response->json()['success']);
    }

    public function test_endpoint_logout()
    {
        $data = [
            'email'    => $this->email,
            'password' => $this->password,
        ];
        $response = $this->post('/api/login', $data);
        $token = $response->json()['authorization']['token'];

        $responseSecond = $this->post('/api/logout', [], ['Authorization' => 'Bearer ' . $token]);
        $responseSecond->assertStatus(200);
    }
}
