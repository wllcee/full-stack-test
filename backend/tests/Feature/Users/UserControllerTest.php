<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithoutMiddleware;

    protected $userId = 1;

    public function setUp(): void
    {
        parent::setUp();
        User::factory()->create(['id' => $this->userId]);
        User::factory(10)->create();
    }

    public function test_endpoint_index()
    {
        $response = $this->get('/api/users');
        $this->assertCount(10, $response->json()['data']);
    }

    public function test_endpoint_show()
    {
        $response = $this->get('/api/users/' . $this->userId);
        $this->assertEquals($this->userId, $response->json()['id']);
    }
}
