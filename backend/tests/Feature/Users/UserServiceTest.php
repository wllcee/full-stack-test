<?php

namespace Tests\Feature\Users;

use App\Models\User;
use App\Services\UserService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserServiceTest extends TestCase
{
    use RefreshDatabase;

    protected $service;
    protected $userId = 1;

    public function setUp(): void
    {
        parent::setUp();
        User::factory()->create(['id' => $this->userId]);
        User::factory(10)->create();
        $this->service = $this->app->make(UserService::class);
    }

    public function test_get_all_users()
    {
        $response = $this->service->getUsers()->toArray();
        $this->assertCount(10, $response['data']);
    }

    public function test_get_users_by_id()
    {
        $response = $this->service->getUser($this->userId)->toArray();
        $this->assertEquals($this->userId, $response['id']);
    }
}
