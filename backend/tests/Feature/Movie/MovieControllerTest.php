<?php

namespace Tests\Feature\Movie;

use App\Models\Movie;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class MovieControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithoutMiddleware;

    protected $movieId = 1;

    public function setUp(): void
    {
        parent::setUp();
        Movie::factory()->create(['id' => $this->movieId]);
        Movie::factory(10)->create();
    }

    public function test_endpoint_index()
    {
        $response = $this->get('/api/movies');
        $this->assertCount(10, $response->json()['data']);
    }

    public function test_endpoint_show()
    {
        $response = $this->get('/api/movies/' . $this->movieId);
        $this->assertEquals($this->movieId, $response->json()['id']);
    }
}
