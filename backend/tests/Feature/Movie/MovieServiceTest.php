<?php

namespace Tests\Feature\Movie;

use App\Models\Movie;
use App\Services\MovieService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MovieServiceTest extends TestCase
{
    use RefreshDatabase;

    protected $service;
    protected $movieId = 1;

    public function setUp(): void
    {
        parent::setUp();
        Movie::factory()->create(['id' => $this->movieId]);
        Movie::factory(10)->create();
        $this->service = $this->app->make(MovieService::class);
    }

    public function test_get_all_movies()
    {
        $response = $this->service->getMovies()->toArray();
        $this->assertCount(10, $response['data']);
    }

    public function test_get_movie_by_id()
    {
        $response = $this->service->getMovie($this->movieId)->toArray();
        $this->assertEquals($this->movieId, $response['id']);
    }

}
