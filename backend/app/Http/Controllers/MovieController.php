<?php

namespace App\Http\Controllers;

use App\Services\MovieService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MovieController extends Controller
{
    protected MovieService $service;

    public function __construct(MovieService $service)
    {
        $this->service = $service;
    }

    public function index(): JsonResponse {
        try {

            return response()->json($this->service->getMovies());

        } catch (Exception $e) {
            
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() ?? Response::HTTP_INTERNAL_SERVER_ERROR);
        
        }
    }

    public function show(int $id): JsonResponse
    {
        try {
            $data = $this->service->getMovie($id);
            
            if(!$data) {
                return response()->json([
                    'success' => false,
                    'message' => "Not Found",
                    'errors' => [
                        "Filme não encontrado."
                    ]
                ], Response::HTTP_NOT_FOUND); ;
            }
            
            return response()->json($data);

        } catch (Exception $e) {
            
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() ?? Response::HTTP_INTERNAL_SERVER_ERROR);

        }
    }
}
