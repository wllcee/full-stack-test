<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    protected UserService $service;

    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    public function index(): JsonResponse {
        try {

            return response()->json($this->service->getUsers());

        } catch (Exception $e) {
            
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() ?? Response::HTTP_INTERNAL_SERVER_ERROR);
        
        }
    }

    public function show(int $id): JsonResponse
    {
        try {
            $data = $this->service->getUser($id);
            
            if(!$data) {
                return response()->json([
                    'success' => false,
                    'message' => "Not Found",
                    'errors' => [
                        "Usuário não encontrado."
                    ]
                ], Response::HTTP_NOT_FOUND); ;
            }
            
            return response()->json($data);

        } catch (Exception $e) {
            
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() ?? Response::HTTP_INTERNAL_SERVER_ERROR);

        }
    }
}
