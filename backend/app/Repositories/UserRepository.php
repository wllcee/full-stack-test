<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\Interfaces\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{

    public function all()
    {
        return User::paginate(10);
    }

    public function find($id)
    {
        return User::find($id);
    }
}
