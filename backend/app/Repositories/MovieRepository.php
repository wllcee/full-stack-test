<?php

namespace App\Repositories;

use App\Models\Movie;
use App\Repositories\Interfaces\MovieRepositoryInterface;

class MovieRepository implements MovieRepositoryInterface
{

    public function all()
    {
        return Movie::paginate(10);
    }

    public function find($id)
    {
        return Movie::find($id);
    }
}
