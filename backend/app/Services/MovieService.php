<?php 

namespace App\Services;

use App\Repositories\MovieRepository;

class MovieService {

    protected MovieRepository $repository;
    
    public function __construct(MovieRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getMovies()
    {
        return $this->repository->all();
    }

    public function getMovie(int $id)
    {
        return $this->repository->find($id);
    }
}