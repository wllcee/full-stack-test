<?php 

namespace App\Services;

use App\Repositories\UserRepository;

class UserService {

    protected UserRepository $repository;
    
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getUsers()
    {
        return $this->repository->all();
    }

    public function getUser(int $id)
    {
        return $this->repository->find($id);
    }
}