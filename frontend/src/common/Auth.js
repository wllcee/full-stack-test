import React from "react";
import api from "../services/api";

const Auth = async () => {
    
    const response = await api.get('/check');
    const isAuth = response.data.success ?? false;
    return isAuth;
}

export default Auth;