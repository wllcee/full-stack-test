import axios from 'axios';

const api = axios.create({
  baseURL: process.env.REACT_APP_BASE_API_URL
});

api.interceptors.request.use(
  function (config) {
    config.headers.Accept = 'application/json';
    config.headers['Content-Type'] = 'application/json';

    if (config.url !== 'login') {
      config.headers.Authorization = `Bearer ${localStorage.getItem('token')}`;

    }
    return config;
  },
);

export default api;