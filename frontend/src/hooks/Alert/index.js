import React from "react";

const Alert = (props) => {
    return (
        <div className="w-100">
            {!!Object.keys(props.messages).length &&
                <div className={`alert alert-${props.status}`} role={'alert'}>
                    {Object.values(props.messages).map((error, index) => <div key={index}>- {error}</div>)}
                </div>
            }
        </div>
    );
};

export default Alert;