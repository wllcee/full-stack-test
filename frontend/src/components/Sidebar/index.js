import React from 'react';

import { Link, useLocation } from 'react-router-dom';


const Sidebar = () => {
    const location = useLocation();

    return (
        <div className="offcanvas offcanvas-start text-bg-dark" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
            <div className="offcanvas-header">
                <h5 className="offcanvas-title" id="offcanvasExampleLabel">Menu</h5>
                <button type="button" className="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div className="offcanvas-body mx-0 px-0">
                <div className="d-flex align-items-start">
                    <ul className="nav nav-tabs flex-column nav-pills w-100" id="myTab" role="tablist">
                        <li className="nav-item" role="presentation">
                            <Link
                                to="/home"
                                name="home"
                                className={`btn btn-dark w-100 ${location.pathname === '/home' ? 'active' : ''}`}

                            >
                                Home
                            </Link>
                        </li>
                        <li className="nav-item" role="presentation">
                            <Link
                                to="/users"
                                name="users"
                                className={`btn btn-dark w-100 ${location.pathname === '/users' ? 'active' : ''}`}

                            >
                                Users
                            </Link>
                        </li>
                        <li className="nav-item" role="presentation">
                            <Link
                                to="/filmes"
                                name="filmes"
                                className={`btn btn-dark w-100 ${location.pathname === '/filmes' ? 'active' : ''}`}

                            >
                                Filmes
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
};

export default Sidebar;