import React from 'react';

import Logo from '../../assets/logo-watch-white.png';
import { Link } from 'react-router-dom';

import './styles.css';

const Header = () => {
    const nameUser = JSON.parse(localStorage.getItem('user'));
    return (
        <header>
            <nav className="navbar navbar-expand-lg bg-dark " data-bs-theme="dark">
                <div className="container-fluid">
                    <div className='separator-right'>
                        <a className="btn btn-dark" data-bs-toggle="offcanvas" href="#offcanvasExample" role="button" aria-controls="offcanvasExample">
                            Menu
                        </a>
                    </div>
                    <div className='mx-4'>
                        <img src={Logo} alt="Logo Watch White" width="100" height="50" />
                    </div>
                    <div className="collapse navbar-collapse ms-5" id="navbarText">
                        <div className='d-flex justify-content-end w-100'>
                            <span className="navbar-text me-5">
                                <ul className="navbar-nav">
                                    {
                                        !!nameUser &&
                                        (
                                            <li className="nav-item dropdown">

                                                <a href="#name" className="nav-link dropdown-toggle" role="button" data-bs-toggle="dropdown">
                                                    {nameUser.name}
                                                </a>

                                                <ul className="dropdown-menu">
                                                    <li>
                                                        <Link className="dropdown-item" to={'/logout'}>
                                                            Sair
                                                        </Link>
                                                    </li>
                                                </ul>
                                            </li>
                                        )
                                    }
                                </ul>
                            </span>
                        </div>
                    </div>
                </div>
            </nav>
        </header>
    );
};

export default Header;