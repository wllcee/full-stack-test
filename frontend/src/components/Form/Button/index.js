import React from "react";

import './styles.css';

const Button = (props) => {
    return (
        <button type="button" className="btn btn-warning button-action" onClick={props.onClick}>{ props.children }</button>
    )
}

export default Button