import React from 'react';
const Pagination = ({currentPage, totalPage, goPage, prev, next}) => {

    const handleGoPage = (value) => {
        goPage(value)
    }
    const renderizarBotoesPagina = () => {

        let buttons = [];

        for (let i = 1; i <= totalPage; i++) {
            buttons.push(
                <button
                    type='button'
                    className="btn btn-dark"
                    key={i}
                    onClick={() => handleGoPage(i)}
                >
                    {i}
                </button>
            );
        }
        return buttons;
    };

    return (
        <div className="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
            <div className="btn-group me-2" role="group" aria-label="First group">
                <button type="button" className="btn btn-dark" onClick={() => handleGoPage(currentPage - 1)} disabled={prev}>Anterior</button>
                {renderizarBotoesPagina()}
                <button type="button" className="btn btn-dark" onClick={() => handleGoPage(currentPage + 1)} disabled={next}>Próxima</button>
            </div>
        </div>
    )
}
export default Pagination;