const FormInput = ({ type = 'text', name, id, placeholder, inputChange }) => {

    const onInputChange = (e) => {
        inputChange(e.target.value)
    }

    return (
        <div className="mb-3 w-100">
            <label htmlFor="email" className={'form-label'}>{name}</label>
            <input
                type={type}
                className={'form-control'}
                id={id}
                placeholder={placeholder}
                onChange={onInputChange}
            />
        </div>
    )
}

export default FormInput