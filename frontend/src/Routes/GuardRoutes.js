import React, { useEffect } from "react";

import { Navigate, useNavigate } from "react-router-dom";
import api from "../services/api";

export function GuardRoute({ children }) {
    const navigate = useNavigate();
    useEffect(() => {
        api.get('/check')
            .then((r) => {
                console.log(r)
            })
            .catch((e) => {
                console.error(e);
                navigate('/login');
            });

    }, []);

    return children

}