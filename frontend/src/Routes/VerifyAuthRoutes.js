import React, { useEffect } from "react";

import { Navigate, useNavigate } from "react-router-dom";
import api from "../services/api";

export function VerifyAuthRoute({ children }) {
    const navigator = useNavigate();
    useEffect(() => {
        api.get('/check')
            .then((r) => {
                console.log(r);
                navigator('/home');
            })
            .catch((e) => {
                console.error(e);
            });
    }, []);

    return children;
}