import React from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import Home from '../pages/Home';
import Users from '../pages/Users';
import Layout from '../pages/Layout';
import Login from '../pages/Auth/Login';
import Logout from '../pages/Auth/Logout';
import Movies from '../pages/Movies/List';
import MovieShow from '../pages/Movies/Show';
import { GuardRoute } from './GuardRoutes';
import { VerifyAuthRoute } from './VerifyAuthRoutes';


const AppRoutes = () => {
  return (
    <Routes>
      <Route path={'/login'} element={<VerifyAuthRoute><Login /></VerifyAuthRoute>} />
      <Route path={'/logout'} element={<GuardRoute><Logout /></GuardRoute>} />
      <Route element={<Layout />} >
        <Route path={'/home'} element={<GuardRoute><Home /></GuardRoute>} />
        <Route path={'users'} element={<GuardRoute><Users /></GuardRoute>} />
        <Route path={'filmes'} element={<GuardRoute><Movies /></GuardRoute>} />
        <Route path={'filme/:id'} element={<GuardRoute><MovieShow /></GuardRoute>} />
        <Route path="*" element={<Navigate to="/login" />} />
      </Route>
    </Routes>
  );
};

export default AppRoutes;