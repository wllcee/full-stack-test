import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import api from '../../../services/api';
import Pagination from '../../../components/Form/Pagination';

import './styles.css';

const Home = () => {

  const apiImage = process.env.REACT_APP_BASE_IMAGE_URL;
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPage, setTotalPage] = useState(1);


  const [movies, setMovies] = useState([]);

  useEffect(() => {
    api.get(`/movies?page=${currentPage}`)
      .then(response => response.data)
      .then(data => {

        setCurrentPage(data.current_page);
        setTotalPage(data.last_page);
        setMovies(data);

      })
      .catch(e => console.error(e.message))
  }, [currentPage]);

  let goPage = (page) => {
    setCurrentPage(page);
  };

  return (
    <div>


      <div className='separator-bottom'>
        <h1>Filmes</h1>
        <p>Lista de filmes!</p>
      </div>
      <table className="table table-borderless">
        <thead>
          <tr>
            <th className={'text-center'}>#</th>
            <th className={'text-center'}>Capa</th>
            <th>Nome</th>
            <th className={'text-center'}>Classificação</th>
            <th className={'text-center'}>Ano</th>
            <th className={'text-center'}>Registro</th>
          </tr>
        </thead>

        <tbody>
          {movies?.data?.map(movie =>
            <tr key={movie.id}>
              <td className={'text-center'}>{movie.id}</td>
              <td className={'text-center'}>
                <img className={'thumb'} src={`${apiImage}${movie.image}`} alt={movie.title} />
              </td>
              <td className={'fw-bold'}>{movie.title}</td>
              <td className={'text-center'}>{movie.classification}</td>
              <td className={'text-center'}>{movie.release_year}</td>
              <td className={'text-center'}>{movie.created_at}</td>
              <td className={'text-center'}>

                <Link to={`/filme/${movie.id}`} className={'btn btn-danger'} >
                  Detalhes
                </Link>
              </td>
            </tr>
          )}
        </tbody>
      </table>
      <div className='d-flex justify-content-center'>

        <Pagination currentPage={currentPage} totalPage={totalPage} goPage={goPage} prev={!movies.prev_page_url} next={!movies.next_page_url} />
      </div>

    </div>
  );
};

export default Home;
