import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';

import Button from '../../../components/Form/Button';
import api from '../../../services/api';
import './styles.css';

const MovieShow = () => {

    const apiImage = process.env.REACT_APP_BASE_IMAGE_URL;
    const navigate = useNavigate()
    const params = useParams()
    const [movie, setMovie] = useState([])

    useEffect(() => {
        api.get(`movies/${params.id}`)
            .then(response => response.data)
            .then(data => {
                setMovie(data)
            })
            .catch(e => console.error(e.message))
    }, [])


    return (
        <div>
            <div className='separator-bottom d-flex justify-content-between'>
                <div>
                    <h1>Detalhes</h1>
                    <p>Página de detalhes do filme!</p>
                </div>
                <div>
                    <div className="d-block text-center p-2">
                        <Button onClick={() => navigate(-1)}>
                            Voltar
                        </Button>
                    </div>
                </div>
            </div>
            <div className="d-flex row justify-content-between direction">
                <div className="w-25 px-2">
                    <img src={`${apiImage}${movie.image}`} alt={movie.title} />
                </div>
                <div className="w-75 px-3">
                    <h5 className={'fw-bold'}>Sinopse</h5>
                    {movie.description}

                    <h6 className={'fw-bold mt-4'}>Ano de Lançamento</h6>
                    {movie.release_year}

                    <h6 className={'fw-bold mt-4'}>Classificação</h6>
                    <span className={'btn btn-secondary fw-bold'}>{movie.classification}</span>
                </div>
            </div>
        </div>
    );
};

export default MovieShow;
