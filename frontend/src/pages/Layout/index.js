import { Outlet } from "react-router-dom";
import Header from "../../components/Header";
import Sidebar from "../../components/Sidebar"

import './styles.css';

const Layout = () => {

    return (
        <div >
            <Header />
            <Sidebar />
            <main className="background-color-layout">
                <div className='d-flex justify-content-center pt-4'>
                    <div className="container-content shadow-sm px-5 py-3">
                        <Outlet />
                    </div>
                </div>
            </main>
        </div>
    );
};

export default Layout;