import React, { useEffect, useState } from 'react';
import api from '../../services/api';
import Pagination from '../../components/Form/Pagination';


const Users = () => {

  const [users, setUsers]             = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPage, setTotalPage]     = useState(1);

  useEffect(() => {
    api.get(`/users?page=${currentPage}`)
      .then(response => response.data)
      .then(data => {

        setCurrentPage(data.current_page);
        setTotalPage(data.last_page);
        setUsers(data);

      })
      .catch(e => console.error(e.message))
  }, [currentPage]);

  let goPage = (page) => {
    setCurrentPage(page);
  };

  return (
    <div>
      <div className='separator-bottom'>
        <h1>Usuários</h1>
        <p>Lista de Usuários!</p>
      </div>
      <table className="table table-borderless">
        <thead>
          <tr>
            <th className={'text-center'}>#</th>
            <th>Nome</th>
            <th>E-mail</th>
            <th className={'text-center'}>Data Cadastro</th>
          </tr>
        </thead>

        <tbody>
          {users?.data?.map(user =>
            <tr key={user.id}>
              <td className={'text-center'}>{user.id}</td>
              <td className={'fw-bold'}>{user.name}</td>
              <td>{user.email}</td>
              <td className={'text-center'}>{user.created_at}</td>
            </tr>
          )}
        </tbody>
      </table>
      <div className='d-flex justify-content-center'>
        <Pagination currentPage={currentPage} totalPage={totalPage} goPage={goPage} prev={!users.prev_page_url} next={!users.next_page_url} />
      </div>
    </div>
  );
};

export default Users;
