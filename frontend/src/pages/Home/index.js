import React from 'react';

const Home = () => {
  return (
    <div>
      <div className='separator-bottom'>
        <h1>Página Inicial</h1>
        <p>Bem-vindo à página inicial!</p>
      </div>
    </div>
  );
};

export default Home;
