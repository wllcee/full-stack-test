import React, { useEffect, useState } from 'react';
import './styles.css';
import FormInput from '../../../components/Form/FormInput';
import Button from '../../../components/Form/Button';
import Alert from '../../../hooks/Alert';

import { useNavigate } from 'react-router';
import api from '../../../services/api';
import Auth from '../../../common/Auth';

const Login = () => {
    const navigate = useNavigate();
    const [errors, setErrors] = useState([]);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const login = async (e) => {
        e.preventDefault();

        api.post(
            '/login',
            {
                email,
                password
            },
            {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }).then((response) => {
                const { data } = response;

                if (data?.success === true) {
                    setErrors([]);
                    localStorage.setItem('user', JSON.stringify(data.user));
                    localStorage.setItem('token', data.authorization.token);
                    navigate('/home');
                }
            }).catch((error) => {
                const { data } = error?.response;

                if (data?.errors) {
                    setErrors(data.errors)
                }

            });
    }

    const handleKeyPress = (e) => {
        if (e.key === 'Enter') {
          login(e);
        }
      };

    return (
        <main className="background-image-layout">
            <div className='container d-flex align-items-center justify-content-center h-100'>
                <div className='container-login bg-dark shadow-lg p-3 mb-5'>

                    <form className='form' action="Auth/Login" onKeyPress={handleKeyPress}>

                        <Alert messages={errors} status="danger" />

                        <h2 style={{ textAlign: 'center' }}>Entrar</h2>

                        <FormInput
                            name={'E-mail'}
                            id={'email'}
                            placeholder={'email@email.com'}
                            value={email}
                            inputChange={value => setEmail(value)}
                        />

                        <FormInput
                            type={'password'}
                            name={'Senha'}
                            id={'password'}
                            placeholder={'******'}
                            value={password}
                            inputChange={value => setPassword(value)}
                        />

                        <Button onClick={login}>
                            Entrar
                        </Button>
                    </form>
                </div>
            </div>
        </main>

    );
};

export default Login;
