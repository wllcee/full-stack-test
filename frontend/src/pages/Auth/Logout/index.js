import { useNavigate } from "react-router-dom";
import api from "../../../services/api";

const Logout = () => {
    const navigate = useNavigate()
    api.post('/logout', {}, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
        }
    }).then(() => {
        localStorage.removeItem("user");
        localStorage.removeItem("token");
        navigate('/login')
    }).catch((error) => {
        navigate('/home')
    })
}

export default Logout